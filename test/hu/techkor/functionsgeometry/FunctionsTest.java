/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.techkor.functionsgeometry;

import java.math.BigInteger;
import java.util.function.DoubleUnaryOperator;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Balazs
 */
public class FunctionsTest {
    
    public FunctionsTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of cube method, of class Functions.
     */
    @Test
    public void testCube() {
        System.out.println("cube");
        int x = 0;
        int expResult = 0;
        int result = Functions.cube(x);
        assertEquals(expResult, result);
        
        x = 10;
        expResult = 1000;
        result = Functions.cube(x);
        assertEquals(expResult, result);
        
        x = 50;
        expResult = 125000;
        result = Functions.cube(x);
        assertEquals(expResult, result);
    }

    /**
     * Test of pow method, of class Functions.
     */
    @Test
    public void testPow() {
        System.out.println("pow");
        int x = 0;
        int y = 1;
        int expResult = 0;
        int result = Functions.pow(x, y);
        assertEquals(expResult, result);
        
        x = 1;
        y = 0;
        expResult = 1;
        result = Functions.pow(x, y);
        assertEquals(expResult, result);
        
        x = 2;
        y = 4;
        expResult = 16;
        result = Functions.pow(x, y);
        assertEquals(expResult, result);
    }

    /**
     * Test of factorial method, of class Functions.
     */
    @Test
    public void testFactorial() {
        System.out.println("factorial");
        int n = 0;
        long expResult = 1L;
        long result = Functions.factorial(n);
        assertEquals(expResult, result);
        
        n = 1;
        expResult = 1L;
        result = Functions.factorial(n);
        assertEquals(expResult, result);
        
        n = 20;
        expResult = 2432902008176640000L;
        result = Functions.factorial(n);
        assertEquals(expResult, result);
    }

    /**
     * Test of vectorLength method, of class Functions.
     */
    @Test
    public void testVectorLength() {
        System.out.println("pow");
        double x = 3.0;
        double y = 4.0;
        double expResult = 5.0;
        double result = Functions.vectorLength(x, y);
        assertEquals(expResult, result, 0.001);
        
        x = -10;
        y = 0;
        expResult = 1;
        result = Functions.vectorLength(x, y);
        assertEquals(expResult, result, 0.001);
        
        x = -1;
        y = 1;
        expResult = Math.sqrt(2);
        result = Functions.vectorLength(x, y);
        assertEquals(expResult, result, 0.001);
    }

    /**
     * Test of distance method, of class Functions.
     */
    @Test
    public void testDistance() {
        System.out.println("distance");
        double x1 = 0.0;
        double y1 = 0.0;
        double x2 = 0.0;
        double y2 = 0.0;
        double expResult = 0.0;
        double result = Functions.distance(x1, y1, x2, y2);
        assertEquals(expResult, result, 0.001);
        
        x1 = 0.0;
        y1 = 0.0;
        x2 = 3.0;
        y2 = 4.0;
        expResult = 5.0;
        result = Functions.distance(x1, y1, x2, y2);
        assertEquals(expResult, result, 0.001);
        
        x1 = 2.0;
        y1 = -3.0;
        x2 = -4.0;
        y2 = 5.0;
        expResult = 10.0;
        result = Functions.distance(x1, y1, x2, y2);
        assertEquals(expResult, result, 0.001);
    }

    /**
     * Test of manhattanDistance method, of class Functions.
     */
    @Test
    public void testManhattanDistance() {
        System.out.println("manhattanDistance");
        double x1 = 0.0;
        double y1 = 0.0;
        double x2 = 0.0;
        double y2 = 0.0;
        double expResult = 0.0;
        double result = Functions.manhattanDistance(x1, y1, x2, y2);
        assertEquals(expResult, result, 0.001);
        
        x1 = 0.0;
        y1 = -3.0;
        x2 = 5.0;
        y2 = 2.0;
        expResult = 10.0;
        result = Functions.manhattanDistance(x1, y1, x2, y2);
        assertEquals(expResult, result, 0.001);
        
        x1 = 2.0;
        y1 = -2.0;
        x2 = 8.0;
        y2 = -8.0;
        expResult = 12.0;
        result = Functions.manhattanDistance(x1, y1, x2, y2);
        assertEquals(expResult, result, 0.001);
    }

    /**
     * Test of insideCircle method, of class Functions.
     */
    @Test
    public void testInsideCircle() {
        System.out.println("insideCircle");
        double r = 1.0;
        double x = 0.0;
        double y = 0.0;
        boolean expResult = true;
        boolean result = Functions.insideCircle(r, x, y);
        assertEquals(expResult, result);
        
        r = 2.0;
        x = 3.0;
        y = 0.0;
        expResult = false;
        result = Functions.insideCircle(r, x, y);
        assertEquals(expResult, result);
        
        r = 1.0;
        x = 0.0;
        y = 1.0;
        expResult = true;
        result = Functions.insideCircle(r, x, y);
        assertEquals(expResult, result);
    }

    /**
     * Test of solveEquations method, of class Functions.
     */
    @Test
    public void testSolveEquations() {
        System.out.println("solveEquations");
        double a11 = 1;
        double a12 = -1;
        double b1 = -1;
        double a21 = 1;
        double a22 = 1;
        double b2 = 25;
        double[] expResult = {12, 13};
        double[] result = Functions.solveEquations(a11, a12, b1, a21, a22, b2);
        assertArrayEquals(expResult, result, 0.001);
        
        a11 = 2;
        a12 = 0;
        b1 = 2;
        a21 = 2;
        a22 = 0;
        b2 = 3;
        result = Functions.solveEquations(a11, a12, b1, a21, a22, b2);
        assertEquals(result.length, 0, 0.001);
        
        a11 = 0;
        a12 = 5;
        b1 = 5;
        a21 = 3;
        a22 = 0;
        b2 = 6;
        double[] expResult3 = {2, 1};
        result = Functions.solveEquations(a11, a12, b1, a21, a22, b2);
        assertArrayEquals(expResult3, result, 0.001);
    }

    /**
     * Test of fastPow method, of class Functions.
     */
    @Test
    public void testFastPow() {
        System.out.println("fastPow");
        BigInteger base = new BigInteger("10");
        BigInteger exponent = new BigInteger("20");
        BigInteger expResult = new BigInteger("100000000000000000000");
        BigInteger result = Functions.fastPow(base, exponent);
        assertEquals(expResult, result);
        
        base = new BigInteger("321456");
        exponent = new BigInteger("5");
        expResult = new BigInteger("3432477361331488865859403776");
        result = Functions.fastPow(base, exponent);
        assertEquals(expResult, result);
        
        base = new BigInteger("41432345678");
        exponent = new BigInteger("6");
        expResult = new BigInteger("5058679076487529899393537031261743031889730764186441745527485504");
        result = Functions.fastPow(base, exponent);
        assertEquals(expResult, result);
    }

    /**
     * Test of numberToString method, of class Functions.
     */
    @Test
    public void testNumberToString() {
        System.out.println("numberToString");
        double number = 10.0;
        int radix = 10;
        String expResult = "10";
        String result = Functions.numberToString(number, radix);
        assertEquals(expResult, result);
        
        number = 10.0;
        radix = 2;
        expResult = "1010";
        result = Functions.numberToString(number, radix);
        assertEquals(expResult, result);
        
        number = 9.375;
        radix = 2;
        expResult = "1001.011";
        result = Functions.numberToString(number, radix);
        assertEquals(expResult, result);
    }

    /**
     * Test of findZero method, of class Functions.
     */
    @Test
    public void testFindZero() {
        System.out.println("findZero");
        DoubleUnaryOperator function = x -> x+10;
        double expResult = -10.0;
        double result = Functions.findZero(function);
        assertEquals(expResult, result, 0.001);
        
        function = x -> Math.atan(x- Math.E);
        expResult = Math.E;
        result = Functions.findZero(function);
        assertEquals(expResult, result, 0.001);
        
        function = x -> (x-4)*(x-4)*(x-4);
        expResult = 4.0;
        result = Functions.findZero(function);
        assertEquals(expResult, result, 0.001);
    }
    
}
