package hu.techkor.functionsgeometry;

import java.math.BigInteger;
import java.util.function.DoubleUnaryOperator;

public class Functions {

    /**
     * Harmadik kitevőre emel.
     *
     * @param x
     * @return
     */
    public static int cube(int x) {
        return 0;
    }

    /**
     * Hatványozás. Kiszámolja x^y-t
     *
     * @param x az alap
     * @param y a kitevő
     * @return x^y
     */
    public static int pow(int x, int y) {
        return 0;
    }
    
    public static long factorial(int n) {
        return 0;
    }
    
    public static double vectorLength(double x, double y) {
        return 0.0;
    }
    
    public static double distance(double x1, double y1, double x2, double y2){
        return 0.0;
    }
            
    public static double manhattanDistance(double x1, double y1, double x2, double y2){
        return 0.0;
    }
                    
    public static boolean insideCircle(double r, double x, double y){
        return false;
    }
    
    public static double[] solveEquations(double a11, double a12, double b1, double a21, double a22, double b2) {
        return new double[2];
    }
    
    public static BigInteger fastPow(BigInteger base, BigInteger exponent) {
        return BigInteger.ZERO;
    }
    
    public static String numberToString(double number, int radix){
        return "";
    }
    
    public static double findZero(DoubleUnaryOperator function) {
        return 0.0;
    }
}
